import itRendersCorrectly from '../it-renders-correctly';

export default (component, mutations) => {
  describe('it renders all mutations', () => {
    mutations.forEach(({ name, props }) => {
      itRendersCorrectly(component, name, props);
    });
  });
};
