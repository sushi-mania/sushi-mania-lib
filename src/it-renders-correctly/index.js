import React from 'react';
import { configure, shallow } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import toJson from 'enzyme-to-json';

configure({ adapter: new Adapter() });

export default (Component, name, props) => {
  test(`it renders correctly ${name}`, () => {
    const component = shallow(<Component {...props} />);

    const tree = toJson(component);

    expect(tree).toMatchSnapshot();
  });
};
